import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'twoDecimals',
  standalone: true
})
export class TwoDecimalsPipe implements PipeTransform {
  transform(value: number): string {
    return value.toFixed(2);
  }
}
