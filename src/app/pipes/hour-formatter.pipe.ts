import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hourFormatter',
  standalone: true
})
export class HourFormatterPipe implements PipeTransform {

  transform(value: number): string {
    if (value < 0 || isNaN(value)) {
      return 'Invalid time';
    }

    const hours = Math.floor(value / 60);
    const minutes = value % 60;
    return `${hours}h ${minutes}m`;
  }

}
