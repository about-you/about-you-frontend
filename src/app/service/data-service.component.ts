import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
import { BigFive } from '../interfaces/big-five';
import { FullReportAnalysis } from '../interfaces/full-report-analysis';
import { Question } from '../interfaces/question';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  private server = "https://superb-passion-production.up.railway.app";
  private gameReport = new BehaviorSubject<FullReportAnalysis | null>(null);
  private musicReport = new BehaviorSubject<FullReportAnalysis | null>(null);
  private formResults = new BehaviorSubject<BigFive | null>(null);
  private questions = new BehaviorSubject<Question[] | null>(null);

  gameReport$ = this.gameReport.asObservable();
  musicReport$ = this.musicReport.asObservable();
  formResults$ = this.formResults.asObservable();
  questions$ = this.questions.asObservable();


  constructor(private http: HttpClient) { }

  setGameReport(report: FullReportAnalysis){
    this.gameReport.next(report);
  }
  getGameReport(): FullReportAnalysis | null {
    return this.gameReport.value;
  }
  setMusicReport(report: FullReportAnalysis){
    this.musicReport.next(report);
  }
  getMusicReport(): FullReportAnalysis | null {
    return this.musicReport.value;
  }
  setFormResult(result: BigFive){
    this.formResults.next(result);
  }
  getFormResult(): BigFive | null {
    return this.formResults.value;
  }
  setQuestions(result: Question[]){
    this.questions.next(result);
  }
  getQuestions(): Question[] | null {
    return this.questions.value;
  }

  fetchQuestions(): Observable<any>{
    return this.http.get(this.server + '/questions');
  }
  analyzeSteam(steamId: string): Observable<any>{
    return this.http.get(this.server + '/steamProfile/' + steamId);
  }
  analyzeSpotify(playlistURL: string): Observable<any>{
    return this.http.post(this.server + '/spotifyPlaylist', playlistURL );
  }
}
