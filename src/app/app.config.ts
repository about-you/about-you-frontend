import { ApplicationConfig } from '@angular/core';
import { Routes, provideRouter } from '@angular/router';
import { QuestionaireComponent } from './components/questionaire/questionaire.component';
import { SteamComponent } from './components/steam/steam.component';
import { OverviewComponent } from './components/overview/overview.component';
import { SpotifyComponent } from './components/spotify/spotify.component';
import { ResultsComponent } from './components/results/results.component';



export const routes: Routes = [
  { path: 'form', component: QuestionaireComponent },
  { path: 'steam', component: SteamComponent },
  { path: 'spotify', component: SpotifyComponent },
  { path: 'results/:type', component: ResultsComponent },
  { path: '', component: OverviewComponent}
];

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes)]
};
