import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../service/data-service.component';
import { FullReportAnalysis } from '../../interfaces/full-report-analysis';
import { ReportType } from '../../enums/report-types';
import { NgIf } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';

@Component({
  selector: 'app-steam',
  standalone: true,
  imports: [ReactiveFormsModule, NgIf, ToastrModule],
  templateUrl: './steam.component.html',
  styleUrl: './steam.component.css'
})


export class SteamComponent implements OnInit {
  buttonText = "ANALYZE";
  form: FormGroup;

  constructor(private router: Router, private dataService: DataService, private fb: FormBuilder){
    if (this.dataService.getQuestions() == null) this.router.navigate(['']);
    this.form = this.fb.group({
      textInput: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.dataService.gameReport$.subscribe(report => {
      if(report) this.router.navigate(['/results', ReportType.GAME_REPORT]);
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.buttonText = "PROCESSING...";
      this.dataService.analyzeSteam(this.form.value.textInput).subscribe((response: FullReportAnalysis) => {
        this.dataService.setGameReport(response);
        this.router.navigate(['/results', ReportType.GAME_REPORT]);
      },
    (error => console.error('Something went wrong. Try again later')));
    } else {
      console.error('Form is invalid');
    }
  }
}
