import { AfterContentInit, Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { NavigationEnd, Router, RouterLink, RouterOutlet } from '@angular/router';
import { DataService } from '../../service/data-service.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-main',
  standalone: true,
  imports: [RouterOutlet, RouterLink, CommonModule],
  templateUrl: './main.component.html',
  styleUrl: './main.component.css'
})
export class MainComponent implements OnInit {
  currentRoute!: string;


  constructor(private dataService: DataService, private renderer: Renderer2, private el: ElementRef, private router: Router){}

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.currentRoute = event.urlAfterRedirects;
      }
    });

    if (!this.dataService.getQuestions()){
      this.dataService.fetchQuestions().subscribe(
        response => {
          this.dataService.setQuestions(response);
        },
        error => {
          console.error("Error fetching questions", error);
        }
      );
    }
  }

  toggleButton(button: HTMLElement): void {
    if(!button.classList.contains('pressed')){
      this.resetButtons();
      button.classList.add('pressed');
    }
  }

  resetButtons(): void {
    const buttons = (this.el.nativeElement as HTMLElement).querySelectorAll('li.button a');
    buttons.forEach(button => {
      this.renderer.removeClass(button, 'pressed');
    })
  }
  isActiveRoute(route: string): boolean {
    console.log(this.currentRoute);
    if(this.currentRoute == "/results/GAME_REPORT" && route == "/steam") {
      return true;
    } else if ( this.currentRoute == "/results/MUSIC_REPORT" && route == "/spotify") {
      return true;
    } else if (this.currentRoute == "results" && route == "/form"){
      return true;
    } else {
      return this.currentRoute === route;
    }
  }

}
