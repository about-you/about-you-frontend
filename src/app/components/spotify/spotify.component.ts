import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ReportType } from '../../enums/report-types';
import { FullReportAnalysis } from '../../interfaces/full-report-analysis';
import { DataService } from '../../service/data-service.component';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-spotify',
  standalone: true,
  imports: [ReactiveFormsModule, NgIf],
  templateUrl: './spotify.component.html',
  styleUrl: './spotify.component.css'
})
export class SpotifyComponent implements OnInit{
  form: FormGroup;
  buttonText = "ANALYZE";

  constructor(private router: Router, private dataService: DataService, private fb: FormBuilder){
    if (this.dataService.getQuestions() == null) this.router.navigate(['']);
    this.form = this.fb.group({
      textInput: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    if(this.dataService.getMusicReport())  this.router.navigate(['/results', ReportType.MUSIC_REPORT]);
  }

  onSubmit() {
    if (this.form.valid) {
      this.buttonText = "PROCESSING...";
      this.dataService.analyzeSpotify(this.form.value.textInput).subscribe((response: FullReportAnalysis) => {
        this.dataService.setMusicReport(response);
      this.router.navigate(['/results', ReportType.MUSIC_REPORT]);
      });
    } else {
      console.error('Form is invalid');
    }
  }
}
