import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FullReportAnalysis } from '../../interfaces/full-report-analysis';
import { BigFive } from '../../interfaces/big-five';
import { DataService } from '../../service/data-service.component';
import { Media } from '../../interfaces/media';
import { PlaylistReport } from '../../interfaces/playlist-report';
import { GameReport } from '../../interfaces/game-report';
import { NgFor, NgIf, TitleCasePipe } from '@angular/common';
import { HourFormatterPipe } from '../../pipes/hour-formatter.pipe';
import { Game } from '../../interfaces/game';
import Chart from 'chart.js/auto';
import { TwoDecimalsPipe } from '../../pipes/two-decimals.pipe';
enum detailLevels{
  LOW, MEDIUM, HIGH
}

@Component({
  selector: 'app-results',
  standalone: true,
  imports: [NgFor, NgIf, TitleCasePipe, HourFormatterPipe, TwoDecimalsPipe],
  templateUrl: './results.component.html',
  styleUrl: './results.component.css'
})

export class ResultsComponent implements OnInit{
  resultFrom: string | null | undefined;
  detailLevel: detailLevels = detailLevels.LOW;
  charts: boolean = false;
  table: boolean = false;
  report: PlaylistReport | GameReport | undefined;
  bigFive: BigFive | undefined;
  items: any[] = [];
  details: any;
  headers: string[] = [];
  chart: any = [];


  constructor(private route: ActivatedRoute, private dataService: DataService, private router: Router){}

  ngOnInit(): void {
    if (this.dataService.getQuestions() == null) this.router.navigate(['']);
    this.route.paramMap.subscribe(params => {
      this.resultFrom = params.get('type');

      if (!this.resultFrom) {
        this.detailLevel = detailLevels.LOW;
        this.bigFive = this.dataService.getFormResult()!;
      }

      if (this.resultFrom == "MUSIC_REPORT") {
        this.detailLevel = detailLevels.MEDIUM;
        const report = this.dataService.getMusicReport();
        this.report = report!.userReport.report as PlaylistReport;
        this.bigFive = report!.analysisResult.bigFive;
        this.items.push(this.report.tracks);
        this.headers = ['cover', 'name', 'album', 'artist', 'popularity', 'danceability', 'energy', 'instrumentalness', 'happiness', 'tempo'];
      }

      if (this.resultFrom == "GAME_REPORT") {
        this.detailLevel = detailLevels.HIGH;
        const report = this.dataService.getGameReport();
        this.report = report!.userReport.report as GameReport;
        this.bigFive = report!.analysisResult.bigFive;
        this.items.push(this.report.topTwenty);
        this.items.push(this.report.lastTwenty);
        this.headers = ['icon', 'name', 'playtime', 'last played', 'tags'];
      }
      if (!this.bigFive) this.router.navigate(['']);

      this.selectComponents();
      this.initializeChart();
    });
  }

  selectComponents(): void {
    switch (this.detailLevel) {
      case detailLevels.LOW:
        //nothing
        break;
      case detailLevels.MEDIUM:
        this.table = true;
        break;
      case detailLevels.HIGH:
        this.table = true;
        this.charts = true;
        break;
    }
  }

  initializeChart(): void {
    this.chart = new Chart('canvas', {
      type: 'bar',
      data: {
        labels: ['Agreeableness', 'Conciousness', 'Extraversion', 'Neuroticism', 'Openness'],
        datasets: [
          {
            label: 'Personality',
            data: [this.bigFive?.AGR, this.bigFive?.CON, this.bigFive?.EXT, this.bigFive?.NEU, this.bigFive?.OPE],
            borderWidth: 1,
            backgroundColor: ['#4CAF50', '#2196F3', '#FF9800', '#F44336', '#9C27B0']
          }
        ]
      },
      options: {
        plugins: {
          legend: {
            display: false
          }
        },
        scales: {
          y: {
            beginAtZero: true,
            suggestedMax: 50
          }
        },
      }
    });
  }

  getKeys(item: any): string[] {
    return Object.keys(item);
  }

  isGameReport(item: any): item is GameReport{
    return item && Array.isArray(item.lastTwenty);
  }

  isPlaylistReport(item: any): item is PlaylistReport{
    return item && Array.isArray(item.tracks);
  }
}
