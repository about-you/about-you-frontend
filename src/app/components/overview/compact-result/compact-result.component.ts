import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { BigFive } from '../../../interfaces/big-five';
import Chart from 'chart.js/auto';
import { NgIf } from '@angular/common';
import { DataService } from '../../../service/data-service.component';

@Component({
  selector: 'app-compact-result',
  standalone: true,
  imports: [NgIf],
  templateUrl: './compact-result.component.html',
  styleUrl: './compact-result.component.css'
})
export class CompactResultComponent implements OnInit, AfterViewInit{

  chart: any = null;

  steamBigFive: BigFive | null = null;
  spotifyBigFive: BigFive | null = null;
  formBigFive: BigFive | null = null;


  constructor(private dataService: DataService){
  }

  ngOnInit(): void {
    this.steamBigFive = this.dataService.getGameReport()!.analysisResult.bigFive;
    this.spotifyBigFive = this.dataService.getMusicReport()!.analysisResult.bigFive;
    this.formBigFive = this.dataService.getFormResult();
  }

  ngAfterViewInit(): void {
    this.initializeChart();
  }

  initializeChart(): void {
    this.chart = new Chart('multi-bar', {
      type: 'bar',
      data: {
        labels: ['Agreeableness', 'Conciousness', 'Extraversion', 'Neuroticism', 'Openness'],
        datasets: [
          {
            label: 'Steam',
            data: [this.steamBigFive?.AGR, this.steamBigFive?.CON, this.steamBigFive?.EXT, this.steamBigFive?.NEU, this.steamBigFive?.OPE],
            borderWidth: 1,
            backgroundColor: ['#66c0f4']
          },
          {
            label: 'Spotify',
            data: [this.spotifyBigFive?.AGR, this.spotifyBigFive?.CON, this.spotifyBigFive?.EXT, this.spotifyBigFive?.NEU, this.spotifyBigFive?.OPE],
            borderWidth: 1,
            backgroundColor: ['#1ed760']
          },
          {
            label: 'Form',
            data: [this.formBigFive?.AGR, this.formBigFive?.CON, this.formBigFive?.EXT, this.formBigFive?.NEU, this.formBigFive?.OPE],
            borderWidth: 1,
            backgroundColor: ['#F46036']
          },
        ]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          }
        },
      }
    });
  }

}
