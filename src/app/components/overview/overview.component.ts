import { NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data-service.component';
import { FullReportAnalysis } from '../../interfaces/full-report-analysis';
import { BigFive } from '../../interfaces/big-five';
import { CompactResultComponent } from './compact-result/compact-result.component';
import { PlaylistReport } from '../../interfaces/playlist-report';
import { GameReport } from '../../interfaces/game-report';

@Component({
  selector: 'app-overview',
  standalone: true,
  imports: [NgIf, CompactResultComponent],
  templateUrl: './overview.component.html',
  styleUrl: './overview.component.css'
})

export class OverviewComponent implements OnInit {
  steamReport: FullReportAnalysis | null = null;
  steamBigFive: BigFive | null = null;
  spotifyReport: FullReportAnalysis | null = null;
  spotifyBigFive: BigFive | null = null;
  formBigFive: BigFive | null = null;



  constructor(private dataService: DataService){}

  ngOnInit(): void {
    this.steamReport = this.dataService.getGameReport();
    this.spotifyReport = this.dataService.getMusicReport();
    this.formBigFive = this.dataService.getFormResult();

    if (this.steamReport) this.steamBigFive = this.steamReport.analysisResult.bigFive;
    if (this.spotifyReport) this.spotifyBigFive = this.spotifyReport.analysisResult.bigFive;

  }

  aptForComparision(): boolean {
    let count = 0;
    if (this.steamReport) ++count;
    if (this.spotifyReport) ++count;
    if (this.formBigFive) ++count;
    return count >= 2;
  }
}

