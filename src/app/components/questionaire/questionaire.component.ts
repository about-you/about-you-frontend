import { NgFor } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../../service/data-service.component';
import { Question } from '../../interfaces/question';
import { Answer } from '../../interfaces/answer';
import { BigFive } from '../../interfaces/big-five';

@Component({
  selector: 'app-questionaire',
  standalone: true,
  imports: [NgFor, ReactiveFormsModule],
  templateUrl: './questionaire.component.html',
  styleUrl: './questionaire.component.css'
})


export class QuestionaireComponent implements OnInit {
  form: FormGroup;
  questionsRaw: any;
  questions: string[] = [];
  possibleResponses: string[] = ["Very Inaccurate",
  "Moderately Inaccurate",
  "Neither Accurate Nor Inaccurate",
  "Moderately Accurate",
  "Very Accurate"];

  constructor(private router: Router, private dataService: DataService, private fb: FormBuilder){
    if (this.dataService.getQuestions() == null) this.router.navigate(['']);
    this.form = this.fb.group({
      answers: this.fb.array([])
    });
  }

  ngOnInit(): void {
    if (this.dataService.getFormResult()) this.router.navigate(['/results', '']);
    this.questionsRaw = this.dataService.getQuestions();
    this.questionsRaw.forEach((questionRaw: Question) => this.questions.push(questionRaw.question));
    this.initializeForm();
  };

  initializeForm(){
    const answersArray = this.form.get('answers') as FormArray;
    this.questions.forEach(() => {
      answersArray.push(this.fb.group({
        answer: ['', Validators.required]
      }));
    });
  }

  get answers(){
    return this.form.get('answers') as FormArray;
  }

  onSubmit() {
    if(this.form.valid){
      const rawResults = this.form.value;
      console.log(rawResults);
      const result = this.processForm(rawResults.answers);

      this.dataService.setFormResult(result)
      this.router.navigate(['/results', '']);
    } else {
      console.error("Form is invalid")
    }

  }

  processForm(rawAnswers: Answer[]){
    let result: BigFive = {
      OPE: 0,
      CON: 0,
      EXT: 0,
      AGR: 0,
      NEU: 0
    }
    let answerValues = rawAnswers.map((answerObj: {answer: string}) => answerObj.answer);

    this.questionsRaw.forEach((rawQ: any, index: number) => {
      let answer = parseInt(answerValues[index]);

      console.log(answer)
      const trait = rawQ.trait;
      const scoring = rawQ.scoring;

      if(scoring === '-'){
        answer = 6 - answer;
      }

      switch (trait){
        case 'OPE':
          result.OPE += answer;
          break;
        case 'CON':
          result.CON += answer;
          break;
        case 'EXT':
          result.EXT += answer;
          break;
        case 'AGR':
          result.AGR += answer;
          break;
        case 'NEU':
          result.NEU += answer;
          break;
      }
    })
    console.log(result)
    return result;
  }


}
