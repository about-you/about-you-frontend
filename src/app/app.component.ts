import { APP_INITIALIZER, Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { DataService } from './service/data-service.component';
import { BrowserModule } from '@angular/platform-browser';
export function initializeApp(dataService: DataService) {
  return () => dataService.fetchQuestions().toPromise();
}

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, MainComponent, HttpClientModule],
  templateUrl: './app.component.html',
  providers: [DataService, {
    provide: APP_INITIALIZER,
    useFactory: initializeApp,
    deps: [DataService],
    multi: true
  }],
  styleUrl: './app.component.css'
})

export class AppComponent {
  title = 'YourAboutPage';
}
