import { FullReport } from "./full-report";
import { Result } from "./result";

export interface FullReportAnalysis {
  userReport: FullReport,
  analysisResult: Result
}
