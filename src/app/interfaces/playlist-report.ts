import { Song } from "./song";

export interface PlaylistReport{
  name: string,
  coverURL: string,
  numberSongs: number,
  averagePopularity: number,
  averageDanceability: number,
  averageEnergy: number,
  averageInstrumentalness: number,
  averageHappiness: number,
  averageTempo: number,
  tracks: Song[]
}
