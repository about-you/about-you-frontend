import { Game } from "./game";

export interface GameReport{
  gamesOwned: number,
  unplayedGames: number,
  topTwenty: Game[],
  lastTwenty: Game[]
}
