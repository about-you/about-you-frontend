import { Media } from "./media";

export interface Game extends Media{

  playtime: number,
  lastTimePlayed: number,
  iconURL: string,
  tags: string[]
}
