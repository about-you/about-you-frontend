export interface Question {
  question: string;
  scoring: string;
  trait: string;
}
