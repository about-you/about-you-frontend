import { Media } from "./media";

export interface Song extends Media{
  album: string,
  artist: string,
  coverURL: string,
  popularity: number,
  danceability: number,
  energy: number,
  instrumentalness: number,
  happiness: number,
  tempo: number
}
