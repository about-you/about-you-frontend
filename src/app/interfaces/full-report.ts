import { ReportType } from "../enums/report-types";
import { GameReport } from "./game-report";
import { PlaylistReport } from "./playlist-report";

export interface FullReport {
  reportType: ReportType,
  report: GameReport | PlaylistReport
}
