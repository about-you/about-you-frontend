export interface BigFive {
  OPE: number,
  CON: number,
  EXT: number,
  AGR: number,
  NEU: number
}
